from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

class InstagramBot:
	def __init__(self, username, password):
		self.username = username
		self.password = password
		self.driver = webdriver.Firefox()

	def closeBrowser(self):
		self.driver.close()

	def login(self):
		driver = self.driver
		driver.get("https://www.instagram.com/?hl=ro")
		time.sleep(2)

		login_button = driver.find_element_by_xpath("//a[@href='/accounts/login/?source=auth_switcher']")
		login_button.click()
		time.sleep(2)

		usernameEl = driver.find_element_by_xpath("//input[@name='username']")
		passwordEl = driver.find_element_by_xpath("//input[@name='password']")

		usernameEl.clear()
		usernameEl.send_keys(self.username)
		passwordEl.clear()
		passwordEl.send_keys(self.password)
		passwordEl.send_keys(Keys.RETURN)
		time.sleep(2)


	def botWork(self, hashtag):
		driver = self.driver
		driver.get("https://www.instagram.com/explore/tags/" + hashtag + "/")
		time.sleep(2)
		#pic hrefs container
		pic_hrefs = []

		#gather the photos
		for i in range(1, 3):
			driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
			time.sleep(2)

		hrefs = driver.find_elements_by_tag_name('a')
		pic_hrefs = [elem.get_attribute('href') for elem in hrefs]
		print hashtag + ' photos: ' + str(len(pic_hrefs))

		selected = 0
		for pic_href in pic_hrefs:
			if selected < 20:
				driver.get(pic_href)
				driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
#				username = driver.find_element_by_class_name("FPmhX notranslate nJAzx")
				username = driver.find_element_by_xpath("//h2[@class='BrX75']/a[1]")
				usr = username.get_attribute('title')
				time.sleep(2)

				driver.get("https://www.instagram.com/" + usr + "/")
				time.sleep(2)
				followers_link = driver.find_element_by_partial_link_text('followers')
				fnr = driver.find_element_by_xpath("//a[@class='" +
					followers_link.get_attribute('class') + "']/span[1]")

				i = 0
				no = ""
				for ch in fnr.get_attribute('title'):
					if ch != ',':
						no += ch

				if int(no) <= 5000:
					print "https://www.instagram.com/" + usr + "/" 
					selected += 1

			
#PASS
davidIG = InstagramBot("david_bsd", "")
davidIG.login()
davidIG.botWork("ecommerce")
